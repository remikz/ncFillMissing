Overview
========

Python script to fill missing values in Netcdf file with nearest (weighted) neighbors. Distance weights are in pixel coordinates, so this technique is intended only for visualization purposes.

A much faster utility to do the same is also in c++ as `ncfill` at sourceforge.net written by me.

Examples
========

	python ncFillMissing.py --vars u --missing -1e20 -n 4 --weighted 4 in.nc out.nc

License
=======

Copyright (C) 2016  Remik Ziemlinski

This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it under the
conditions of the GPLv3 license.

