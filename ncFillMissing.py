#!/usr/bin/env python

from functools import partial
from math import sqrt
from netCDF4 import Dataset

import sys
import optparse
import rtree

optparse.OptionParser.format_epilog = lambda self, formatter: self.epilog


class NcFiller:
    def __init__(self, options ):
        self.options = options

        self.ncIn = None
        self.ncOut = None

    def closeFiles( self ):
        if self.ncIn:
            self.ncIn.close()

        if self.ncOut:
            self.ncOut.close()

    def getAverageProcessor(self):
        if self.options.weighted:
            return partial(computeWeightedAverage, p=self.options.weighted)
        else:
            return computeAverage

    def initOutput(self):
        # Clone dims.
        for dim in self.ncIn.dimensions:
            self.ncOut.createDimension( dim, len( self.ncIn.dimensions[dim] ) )

        # Clone vars.
        for name in self.ncIn.variables:
            varIn = self.ncIn.variables[ name ]

            dtype = self.options.types.get( name, varIn.dtype )

            if hasattr( varIn, '_FillValue' ):
              fillValue = varIn._FillValue
            else:
              fillValue = None

            varOut = self.ncOut.createVariable( name, dtype, varIn.dimensions, fill_value=fillValue )

            for att in dir( varIn ):
              if att in ['_FillValue']: continue

              if type(att) == unicode:
                exec( 'varOut.{att} = varIn.{att}'.format(att=att) )

    def openFiles( self ):
        self.ncIn = Dataset( self.options.input )
        self.ncOut = Dataset( self.options.output, 'w' )

    def processVar(self, name):
        ndims = len( self.ncIn.variables[ name ].dimensions )
        varIn = self.ncIn.variables[ name ]
        varOut = self.ncOut.variables[ name ]
        varIn.set_auto_maskandscale(False)

        # Assume static missing mask.
        if 4 == ndims:
            matrix = varIn[0, 0, ...]
            fun = self.processVar4d
        elif 3 == ndims:
            matrix = varIn[0, ...]
            fun = self.processVar3d
        else:
            matrix = varIn[...]
            fun = self.processVar2d

        tree, treeCoords = buildValidPixelTree( matrix, self.options.missing )
        missingCoords = findMissing( matrix, self.options.missing )
        avgFun = self.getAverageProcessor()
        # open('/tmp/missingCoords.txt','w').write( str( missingCoords) )

        fun(tree, treeCoords, missingCoords, varIn, varOut, avgFun)

    def processVar4d(self, tree, treeCoords, missingCoords, varIn, varOut, avgFun):
        nt = varIn.shape[0]
        nz = varIn.shape[1]
        for tIndex in xrange(nt):
            print 'Processing t={0}/{1}'.format( tIndex+1, nt )
            for zIndex in xrange(nz):
                if self.options.progress:
                    print 'Processing z={0}/{1}'.format( zIndex+1, nz )

                matrix = varIn[tIndex, zIndex, ...]
                fillMissing2d(matrix, tree, treeCoords, missingCoords,
                              self.options.neighbors, avgFun)
                varOut[ tIndex, zIndex ] = matrix

    def processVar3d(self, tree, treeCoords, missingCoords, varIn, varOut, avgFun):
        nz = varIn.shape[0]
        for zIndex in xrange(nz):
            if self.options.progress:
                print 'Processing z={0}/{1}'.format( zIndex+1, nz )

            matrix = varIn[tIndex, zIndex, ...]
            fillMissing2d( matrix, tree, treeCoords, missingCoords, self.options.neighbors, avgFun )
            varOut[ zIndex ] = matrix

    def processVar2d(self, tree, treeCoords, missingCoords, varIn, varOut, avgFun):
        matrix = varIn[...]
        fillMissing2d( matrix, tree, treeCoords, missingCoords, self.options.neighbors, avgFun )
        varOut[:] = matrix

    def processVars(self):
        for var in self.options.vars:
            self.processVar( var )

    def run(self):
        self.openFiles()
        self.initOutput()
        self.processVars()
        self.closeFiles()

def buildValidPixelTree( matrix, missingValue ):
    '''
    Return ij coordinate RTree for all pixels that do not have invalid values,
    and the list of coords.
    @param matrix 2d matrix.
    @param invalid What values to ignore.
    '''
    idx = rtree.index.Index()
    coords = [] # The inserted coords for lookup later.

    ny, nx = matrix.shape[-2:]
    nInserted = 0

    for i in xrange( ny ):
        for j in xrange( nx ):
            if missingValue <> matrix[ i, j ]:
                # Insert (left, bottom, right, top).
                idx.insert(nInserted, (j,i,j,i) )
                coords.append( (i,j) )
                nInserted += 1

    return idx, coords

def computeAverage( i, j, matrix, nearestCoords ):
    '''
    Returns single value that is average for point at
    position i,j in matrix, using 'nearestCoords' for weight inputs.
    '''
    total = 0.

    for i2,j2 in nearestCoords:
        total += matrix[ i2, j2 ]

    result = total / len( nearestCoords )

    return result

def computeWeightedAverage( i, j, matrix, nearestCoords, p=2 ):
    '''
    Returns single value that is p=2 distance weighted average for point at
    position i,j in matrix, using 'nearestCoords' for weight inputs.
    '''
    u = 0.
    sumw = 0.

    for i2,j2 in nearestCoords:
        d = sqrt( (i-i2)**2 + (j-j2)**2 )
        w = 1. / d**p
        u += w * matrix[ i2, j2 ]
        sumw += w

    return u / sumw

def fillMissing2d(matrix, tree, treeCoords, missingCoords, nNearest, computeAvg):
    '''
    @param tree Rtree of valid pixel coords without missing value.
    @param missingCoords List of (y,x) coords for pixels that had missing value.
    '''
    for i,j in missingCoords:
        nearestObjs = tree.nearest( (j,i,j,i), nNearest )
        nearestIds = list( nearestObjs )
        nearestCoords = [ treeCoords[idx] for idx in nearestIds ]
        newValue = computeAvg( i, j, matrix, nearestCoords )
        matrix[ i, j ] = newValue

def findMissing( matrix, missingValue ):
    '''
    Returns list of (y,x) pixel coords where missing value is found.
    '''
    result = []
    ny, nx = matrix.shape[-2:]

    for i in xrange( ny ):
        for j in xrange( nx ):
            # print '%.17g' % matrix[ i, j ], missingValue == matrix[ i, j ]
            if missingValue == matrix[ i, j ]:
                result.append( (i,j) )

    return result

def parseOptions( argv ):
    description = 'Fill missing values with nearest neighbor (weighted) averages for Netcdf files. Assumes static grid masks.'
    examples = '''
Examples:
    ./ncFillMissing --missing -1e20 --progress --vars u,v in.nc out.nc
'''
    usage = 'Usage: %prog [options] in.nc out.nc'

    parser = optparse.OptionParser(description=description,
                                   epilog=examples,
                                   usage=usage)

    parser.add_option('-d', '--debug', help='Print debug messages.',
                      action="store_true")
    parser.add_option('-i', '--input', help='Input file.')
    parser.add_option('-m', '--missing', help='Missing value to fill.')
    parser.add_option('-n', '--neighbors',
        help='Number of nearest values to use. Default=1',
        default=1)
    parser.add_option('-o', '--output', help='Output file.')
    parser.add_option('-p', '--progress', help='Print progress.',
                      action="store_true")
    parser.add_option('-t', '--types', help='Output type override. For example, -t SST:f,TIME:d')
    parser.add_option('-v', '--vars', help='Variables to process. Comma delimited.')
    parser.add_option('-w', '--weighted',
                      help='Use inverse pixel distance weighting exponent p. Default is off.',
                      default=0)

    opts, args = parser.parse_args( argv )

    if not opts.input:
        if len(args) < 2:
            parser.error('--input missing')
        else:
            opts.input = args[0]

    if not opts.output:
        if len(args) < 2:
            parser.error('--output missing')
        else:
            opts.output = args[1]

    if not opts.vars:
        parser.error('--vars missing')

    opts.vars = opts.vars.split(',')
    opts.weighted = int(opts.weighted)

    types = {}
    if opts.types:
        for pair in opts.types.split(','):
            key, val = pair.split(':')
            types[key] = val

    opts.types = types

    if opts.debug:
        print opts

    try:
      opts.missing = float( opts.missing )
    except: pass

    opts.neighbors = int( opts.neighbors )

    return opts

def main( argv ):
    options = parseOptions( argv )

    filler = NcFiller( options )
    filler.run()


if __name__ == '__main__':
    main( sys.argv[1:] )


#     LICENSE BEGIN
#
#     ncFillMissing
#     Copyright (C) 2016  Remik Ziemlinski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#     LICENSE END
